import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler,StandardScaler,RobustScaler
from sklearn import linear_model,neighbors
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error,r2_score
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestRegressor,AdaBoostRegressor
from ESN import classicESN, classicTuner
from sklearn.svm import NuSVR
import matplotlib.pyplot as plt
import matplotlib
import AutoEncode as AE
import SVR
matplotlib.style.use('ggplot')
from sklearn.neural_network import MLPRegressor
from GLOBALS import *
import ANN
from sklearn.model_selection import cross_val_score
from sklearn.feature_selection import RFE,RFECV
class Model():
    """Responsible to fit a mathematical model
    """

    def __init__(self):
        self.task = "Responsible to fit a mathematical model"

    def readDataset(self):
        """Reads the dataset"""

        self.dataSet = pd.read_csv("Data/cleanData.csv")
        self.features = self.dataSet.columns.values.tolist()
        self.inputFeatures,self.outputFeatures = self.features[:-1],self.features[-1:]

        self.dataSet = self.dataSet.as_matrix()
        self.dataSet = self.shuffleData ( self.dataSet )                    #Shuffle the data
        self.splitTrainTest()                                               #Split as test and train sets
        self.ScaleData()                                                    #Decorrelate and scale the data
        #self.DimReduction(Dim=7)
        self.dataForSV()

        return None



    def dataForSV(self):
        """combines test and train for cross validation"""

        trainX, trainY, testX, testY = self.splitInputOutput()
        self.fullX = np.vstack((trainX,testX))
        self.fullY = np.vstack((trainY,testY))

        return None


    def DimReduction(self,Dim = 4):
        """Perform Dimensionality reduction using PCA"""

        trainX, trainY, testX, testY = self.splitInputOutput()

        Obj = PCA(n_components=Dim)
        trainX = Obj.fit_transform(trainX)
        testX = Obj.transform(testX)
        self.mergeInputOutput(trainX, trainY, testX, testY)

        return None

    def mergeInputOutput(self,trainX, trainY, testX, testY):
        """Merges the input and output features where output feature is always at the end"""

        self.train = np.hstack((trainX,trainY))
        self.test = np.hstack((testX,testY))

        return None

    def splitTrainTest(self, trainRatio = 0.999):
        """Splits the dataset as train and test set based on the ratio specified
        trainRatio : specify the ratio of points to remain in training set"""

        splitIndex = int(self.dataSet.shape[0] * trainRatio)
        self.train, self.test = self.dataSet[:splitIndex],self.dataSet[splitIndex:]
        #self.train, self.test = self.dataSet,self.dataSet
        return None

    def shuffleData(self,Input):
        """Shuffles the dataset"""

        shuffler = np.arange(Input.shape[0])
        np.random.shuffle(shuffler)

        return Input[shuffler]
    def featureRanker(self,ranks):
        """Based on the ranks at feature index, we print the features with highest ranks"""

        rankingMap = np.vstack((ranks,self.inputFeatures)).T
        rankingMap = pd.DataFrame(rankingMap,columns=['ranks','features']).sort_values(['ranks'])
        print "Important features are , ",rankingMap['features'].as_matrix()

        return None

    def ScaleData(self,Type = "MinMax"):
        """Scales the data
        Type = 'unitVar' or 'MinMax'"""

        if(Type == 'MinMax'):
            Scaler = MinMaxScaler(feature_range=(-1.0,1.0))
        elif(Type == 'unitVar'):
            Scaler = StandardScaler()
        elif(Type == 'Robust'):
            Scaler = RobustScaler()

        self.train = Scaler.fit_transform(self.train)
        self.test = Scaler.transform(self.test)
        #For CV
        self.dataSet = Scaler.fit_transform(self.dataSet)

        #For converting all data points as convex combination of extremes
        if(ARCHETYPAL_ENCODE):
            trainX, trainY, testX, testY = self.splitInputOutput()
            AAObj = AE.AutoEncode(maxoids=EXTREMES)
            trainX = AAObj.fit_transform(trainX)
            testX = AAObj.transform(testX)
            self.fullX = AAObj.fit_transform(self.fullX)
            print 'transformed Inputs'
            self.mergeInputOutput(trainX, trainY, testX, testY)
        return None

    def LinearRegression(self):
        """Use Linear Regression to learn the underlying model"""

        trainX, trainY, testX, testY = self.splitInputOutput()
        LR = linear_model.LinearRegression()
        LR.fit(trainX,trainY)
        predictedY = LR.predict(testX)
        print "\n\nUsing Linear Regressor"
        print "Learned Coeffcients \t", LR.coef_
        print "RMSE: \t",np.sqrt(mean_squared_error(testY, predictedY))
        print "R2 Score: \t",r2_score(testY, predictedY)

        scores = cross_val_score(LR, self.fullX, self.fullY, cv=10,scoring='r2')
        print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

        #plot true and predicted
        self.plotPrediction("Linear Regressor",testY,predictedY)

        return None


    def splitInputOutput(self):
        """Splits the dataset as input and output matrices
        Assuming the output or target feature is the last column"""

        trainX, trainY = self.train[:,:(self.train.shape[1]-1)],self.train[:,(self.train.shape[1]-1):]
        testX, testY = self.test[:,:(self.test.shape[1]-1)],self.test[:,(self.test.shape[1]-1):]

        return trainX, trainY, testX, testY

    def plotPrediction(self, ModelName, true, predicted):
        """Plots the values of true and predicted price"""

        df = pd.DataFrame(data=np.hstack((true, predicted)) , columns= ['true','predicted'])

        #Sort according to true price
        df = df.sort_values(['true'])

        #reset index after sort
        df = df.reset_index(drop=True)

        f = plt.figure()
        # ax = f.add_subplot(111)
        df.plot()
        plt.title('Random Forest')
        #plt.title(ModelName + (" (RMSE : %0.2f , R2 : %0.2f)" % (rmse,r2)))
        plt.savefig("Error plots/"+str(ModelName)+".jpg")
        return None


    def KnnRegressor(self):
        """Perform Knn for Regression"""

        trainX, trainY, testX, testY = self.splitInputOutput()

        print "\n\nUsing Knn..."
        KnnObj = neighbors.KNeighborsRegressor()
        paramater_grid = dict(n_neighbors = range(5,20),weights=['uniform','distance'])

        #For tuning the no. of neighbors parameter
        grid = GridSearchCV(KnnObj,paramater_grid,cv=3,scoring='r2',n_jobs=-1,verbose=0)
        grid.fit(trainX,trainY)
        predictedY = grid.predict(testX)
        print "Choosen Parameters: \t",grid.best_params_

        # scores = cross_val_score(neighbors.KNeighborsRegressor(n_neighbors=grid.best_params_['n_neighbors'],weights=grid.best_params_['weights']), self.fullX, self.fullY, cv=10,scoring='r2')
        # print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

        print "RMSE: \t",np.sqrt(mean_squared_error(testY, predictedY))
        print "R2 Score: \t",r2_score(testY, predictedY)

        #plot true and predicted
        self.plotPrediction("Knn",testY,predictedY)



        return None

    def ESN(self):
        """Using an Echo State Network"""

        trainX, trainY, testX, testY = self.splitInputOutput()
        reservoirSize = 100
        # Obj = classicTuner.ReservoirTuner(size=reservoirSize,initialTransient=50,trainingInputData=trainX,trainingOutputData=trainY,validationInputData=testX,validationOutputData=testY,spectralRadiusBound=(0.2,0.9),inputScalingBound=(0.2,0.9),reservoirScalingBound=(0.2,0.9),leakingRateBound=(0.9,1.0))
        # result = Obj.getOptimalParameters()
        # print result

        #ESNObj = classicESN.Reservoir(trainX,trainY,size=reservoirSize,spectralRadius=result[0],inputScaling=result[1],reservoirScaling=result[2],leakingRate=result[3],inputWeightRandom=result[4],reservoirWeightRandom=result[5])
        ESNObj = classicESN.Reservoir(trainX,trainY,size=reservoirSize)
        ESNObj.trainReservoir()
        predictedY = ESNObj.predict(testX)


        print "\n\nUsing Echo State Network"
        print "RMSE: \t",np.sqrt(mean_squared_error(testY, predictedY))
        print "R2 Score: \t",r2_score(testY, predictedY)

        #plot true and predicted
        self.plotPrediction("ESN",testY,predictedY)

        return None

    def RandomForest(self):
        """Use RandomForest Regressor"""

        trainX, trainY, testX, testY = self.splitInputOutput()

        print "\n\nUsing Random Forest..."
        RfObj = RandomForestRegressor()
        paramater_grid = dict(n_estimators = range(5,100))

        #For tuning the no. of neighbors parameter
        grid = GridSearchCV(RfObj,paramater_grid,cv=3,scoring='r2',n_jobs=-1,verbose=0)
        grid = RandomForestRegressor(n_estimators=131)
        grid.fit(trainX,trainY)
        predictedY = grid.predict(testX).reshape(testY.shape)

        #print "Choosen Parameters: \t",grid.best_params_
        print "RMSE: \t",np.sqrt(mean_squared_error(testY, predictedY))
        print "R2 Score: \t",r2_score(testY, predictedY)

        #plot true and predicted
        self.plotPrediction("RF",testY,predictedY)

        return None

    def SVR(self):
        """Performs support vector regression"""

        trainX, trainY, testX, testY = self.splitInputOutput()

        print "\n\nUsing Support Vector Regression"

        #For tuning parameters
        SVRObj = SVR.SVR(training_features=trainX,training_target=trainY)
        #result = SVRObj.getbestparameter()

        result = [0.9,0.5]
        Obj = NuSVR(nu=result[1],gamma=result[0],C=1.0)
        Obj.fit(trainX,trainY)
        predictedY = Obj.predict(testX).reshape(testY.shape)

        print "Choosen Parameters: \t",result



        parameters = "gamma: "+str(result[0])+ " kernel: "+str("rbf")
        rmse = np.sqrt(mean_squared_error(testY, predictedY))
        r2 = r2_score(testY, predictedY)
        print "RMSE: \t",rmse
        print "R2 Score: \t",r2

        #plot true and predicted
        self.plotPrediction("SVR "+ parameters,testY,predictedY,rmse,r2)

        #plot true and predicted
        #self.plotPrediction("SVR",testY,predictedY)

        return None

    def MLP(self):
        """use MLP from sklearn"""


        trainX, trainY, testX, testY = self.splitInputOutput()

        print "\n\nUsing MLP"
        MLPObj = ANN.ANN()
        predictedY = MLPObj.trainPredict(trainX, trainY, testX, testY).reshape(testY.shape)

        print "RMSE: \t",np.sqrt(mean_squared_error(testY, predictedY))
        print "R2 Score: \t",r2_score(testY, predictedY)

        #plot true and predicted
        self.plotPrediction("MLP",testY,predictedY)

        return None




    def RFE(self):
        """Do Feature Elimination recursively"""

        print "performing recursive feature elimination"
        trainX, trainY, testX, testY = self.splitInputOutput()
        estimatorObj = RandomForestRegressor(n_estimators=131)
        Obj = RFE(estimator=estimatorObj,n_features_to_select=1,verbose=1)
        Obj.fit(trainX,trainY)
        self.featureRanker(np.array(Obj.ranking_))


        return None



if __name__ == "__main__":

    Obj = Model()
    Obj.readDataset()
    Obj.LinearRegression()
    Obj.KnnRegressor()
    Obj.ESN()
    Obj.RandomForest()
    Obj.SVR()
    Obj.MLP()
    Obj.AdaBoostRegression()