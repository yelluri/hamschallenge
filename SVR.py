from sklearn.metrics import mean_squared_error
from sklearn.svm import NuSVR
from scipy import optimize


#For tuning the parameters

class SVR:
    def __init__(self, training_features, training_target, initial_guess = None,
                 parameter_bounds=[(0.1, 1.0), (0.1, 0.99)],
                 c=1.0):
        """initial guess for gamma and number of SV"""

        self.training_features = training_features
        self.training_target = training_target

        if initial_guess is None:
            self.initial_guess = [0.9, 0.7]  # Default values
        else:
            self.initial_guess = initial_guess

        self.parameter_bounds = parameter_bounds
        self.c = c
        self.splitValidationSet()

    def splitValidationSet(self,trainRatio=0.85):
        """Splits the data as train and validation sets for tuning"""

        splitIndex = int(self.training_features.shape[0] * trainRatio)
        self.trainX, self.valX = self.training_features[:splitIndex],self.training_features[splitIndex:]
        self.trainY, self.valY = self.training_target[:splitIndex],self.training_target[splitIndex:]

        return None

    def __objective__(self, x):  # Minimizes the training error

        # Extract the parameters
        gamma = x[0]
        number_of_sv = x[1]

        # Train the model
        Obj = NuSVR(nu=number_of_sv,C=self.c,kernel='rbf',degree=3,gamma=gamma)
        Obj.fit(self.trainX,self.trainY)
        predicted = Obj.predict(self.valX)

        # predictor_obj = Predictor(features=self.training_features, target=self.training_target,
        #                           start_of_horizon=None, horizon=None,
        #                           gamma=gamma, c=self.c, number_of_sv=number_of_sv)
        # predicted = predictor_obj.predict_with_features(features=self.training_features)

        # Calculate regression error
        error = mean_squared_error(self.valY, predicted)
        print error,x
        # Return the error
        return error

    def __tune__(self):
        result = optimize.minimize(x0=self.initial_guess, bounds=self.parameter_bounds, fun=self.__objective__, options={'maxiter': 1,'eps': 0.01})
        return result.x[0], result.x[1]

    def getbestparameter(self):
        # Tune and return the best parameter
        return self.__tune__()