#For encoding weekdays as k- hot encoding
ENCODE_WEEKDAY = False

#Perform Archetypal analysis on the input features and represent them as convex combination of extremes
ARCHETYPAL_ENCODE = False
if(ARCHETYPAL_ENCODE):
    EXTREMES = 70

#para4 has a special case for value 13.6, we can flag this in an indicator variable
INDICATOR_PARA4 = False