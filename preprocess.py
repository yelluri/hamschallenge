import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('ggplot')
from keras.utils import np_utils
from GLOBALS import *
import Model,ModelOld,FinalModel
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler,StandardScaler,RobustScaler
class preprocess():
    """To clean and preprocess the data
    """

    def __init__(self):
        self.task = "Preprocess the dataset"

    def readDataset(self):
        """Reads the file"""

        self.dataSet = pd.read_csv("Data/sample.csv")

        return None

    def handleweekdays(self):
        """Responsible to convert weekdays from string to numbers"""

        days = { 'Mon' : 0, 'Tue' : 1, 'Wed' : 2, 'Thu' : 3, 'Fri' : 4, 'Sat' : 5, 'Sun' : 6}
        self.dataSet['dow'] = self.dataSet['dow'].apply(lambda x: days[x])

        #for k hot encoding
        if(ENCODE_WEEKDAY):
            encoder = np_utils.to_categorical(self.dataSet['dow'], 7)
            encoder = pd.DataFrame(encoder,columns=['Mon','Tue','Wed','Thu','Fri','Sat','Sun'])
            self.dataSet = pd.concat([encoder,self.dataSet],axis=1)
            del (self.dataSet['dow'])

        #Indicates the special value for para4 at 13.6
        if(INDICATOR_PARA4):
            indicator = pd.DataFrame(np.where(self.dataSet['para4'] == 13.6,1,0),columns=['indpara4'])
            self.dataSet = pd.concat([indicator,self.dataSet],axis=1)


        return None

    def clean(self):
        """Removes erroneos string data where all data is supposed to be numerical"""

        #For non real values, it sets NaN
        self.dataSet['loc1'] = pd.to_numeric(self.dataSet['loc1'],errors='coerce')
        self.dataSet['loc2'] = pd.to_numeric(self.dataSet['loc2'],errors='coerce')

        #Drops rows with NaN values
        self.dataSet = self.dataSet[np.isfinite(self.dataSet['loc1'])]
        self.dataSet = self.dataSet[np.isfinite(self.dataSet['loc2'])]

        #Convert the type of features as int (as it was int originally)
        self.dataSet['loc1']  =self.dataSet['loc1'].astype(int)
        self.dataSet['loc2']  =self.dataSet['loc2'].astype(int)

        #Since the 10's unit is already in loc1, we just retain 1's unit
        self.dataSet['loc2'] = self.dataSet['loc2'].apply(lambda x: x%10)

        #Drop outliers were value of para1 has very less support
        self.dataSet = self.dataSet.ix[self.dataSet['para1'] < 6]

        #self.ScaleData()
        #self.plot3D()
        return None



    def saveDataset(self):
        """Save the cleaned and processed data as CSV"""

        self.dataSet.to_csv("Data/cleanData.csv",index=False)

        return None


    def ScaleData(self, Type= 'MinMax'):
        """Scales or Normalizes the data
        Also preforms PCA Dim reduction"""
        if(Type == 'MinMax'):
            Scaler = MinMaxScaler(feature_range=(-1.0,1.0))
        elif(Type == 'unitVar'):
            Scaler = StandardScaler()
        elif(Type == 'Robust'):
            Scaler = RobustScaler()

        self.dataSet = Scaler.fit_transform(self.dataSet)

        #For dim reduction using PCA, helps in visualizing in 3D space
        self.dimRed()
        return None


    def dimRed(self, dim = 2):
        """Perform Dim red using PCA"""

        x, y = self.dataSet[:,:(self.dataSet.shape[1]-1)],self.dataSet[:,(self.dataSet.shape[1]-1):]
        Obj = PCA(n_components=dim)
        x = Obj.fit_transform(x)
        self.dataSet = np.hstack((x,y))
        self.dataSet = pd.DataFrame(self.dataSet,columns=['dim1','dim2','price'])

        return None

    def plot3D(self):
        """plot the points in 3D scatter plot"""

        df = self.dataSet
        threedee = plt.figure().gca(projection='3d')

        x = 'dim1'
        y = 'dim2'
        z = 'price'

        threedee.scatter(df[x], df[y], df[z])
        threedee.set_xlabel(x)
        threedee.set_ylabel(y)
        threedee.set_zlabel(z)
        plt.show()

        return None

    def analyseFeatures(self):
        """Computes the correlation coefficient for each feature with all others"""

        #Computing the correlation coefficient for every feature with each other
        CorrMatrix = self.dataSet.corr(method='spearman')

        return None

if __name__ == "__main__":


    Obj = preprocess()
    Obj.readDataset()
    Obj.handleweekdays()
    Obj.clean()
    Obj.analyseFeatures()
    Obj.saveDataset()

    #exit()

    Obj = Model.Model()
    Obj.readDataset()
    #Obj.LinearRegression()
   # Obj.KnnRegressor()
    Obj.RandomForest()
    #Obj.SVR()
    #Obj.MLP()
    #Obj.RFE()



