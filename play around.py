import numpy as np
import pandas as pd
import cPickle
import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('ggplot')
from keras.utils import np_utils
from GLOBALS import *
import os
import Model,ModelOld
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler,StandardScaler,RobustScaler
import seaborn as sns

class preprocess():
    """To clean and preprocess the data
    """

    def __init__(self):
        self.task = "Preprocess the dataset"

    def readDataset(self):
        """Reads the file"""

        self.dataSet = pd.read_csv("Data/sample.csv")

        return None

    def handleweekdays(self):
        """Responsible to convert weekdays from string to numbers"""

        days = { 'Mon' : 0, 'Tue' : 1, 'Wed' : 2, 'Thu' : 3, 'Fri' : 4, 'Sat' : 5, 'Sun' : 6}
        self.dataSet['dow'] = self.dataSet['dow'].apply(lambda x: days[x])


        return None

    def clean(self):
        """Removes erroneos string data where all data is supposed to be numerical"""

        #For non real values, it sets NaN
        self.dataSet['loc1'] = pd.to_numeric(self.dataSet['loc1'],errors='coerce')
        self.dataSet['loc2'] = pd.to_numeric(self.dataSet['loc2'],errors='coerce')

        #Drops rows with NaN values
        self.dataSet = self.dataSet[np.isfinite(self.dataSet['loc1'])]
        self.dataSet = self.dataSet[np.isfinite(self.dataSet['loc2'])]

        #Convert the type of features as int (as it was int originally)
        self.dataSet['loc1']  =self.dataSet['loc1'].astype(int)
        self.dataSet['loc2']  =self.dataSet['loc2'].astype(int)



        #Since the 10's unit is already in loc1, we just retain 1's unit
        #self.dataSet['loc2'] = self.dataSet['loc2'].apply(lambda x: x%10)

        # self.dataSet = self.dataSet.ix[self.dataSet['loc2'] == 25]
        # self.dataSet = self.dataSet.sort_values(['para3'])
        # self.dataSet =  self.dataSet[self.dataSet['para2'] == 309]
        #
        # self.dataSet =  self.dataSet[self.dataSet['para3'] == 18000.0]
        #
        # print self.dataSet





        # exit()
        # feature = 'para2'
        # grouped = self.dataSet.groupby(feature)
        # means = grouped.mean()
        # errors = grouped.std()
        # means.price.plot.bar(yerr=errors["price"], title="Price")
        # plt.show()

        #Computing the correlation coefficient for every feature with each other

        #self.dataSet['para4'] =   (self.dataSet['para2']) *(self.dataSet['para4'])
        #self.dataSet['para2'] =   (self.dataSet['para2']) *(self.dataSet['para4'])
        self.dataSet.plot.scatter(x="para2",y="price", alpha=0.2)
        plt.show()
        #self.dataSet = self.dataSet[['para1','para2','para4','para3','price']]
        #self.ScaleData()
        #self.plot3D()

        #self.createHeatMap(CorrMatrix=0)
        return None

    def createHeatMap(self,matrix):
        """Creates a heatmap for the correlation or covariance matrix"""

        ax = sns.heatmap(matrix)
        plt.show()

        return None


    def ScaleData(self, Type= 'MinMax'):
        """Scales or Normalizes the data"""
        if(Type == 'MinMax'):
            Scaler = MinMaxScaler(feature_range=(-1.0,1.0))
        elif(Type == 'unitVar'):
            Scaler = StandardScaler()
        elif(Type == 'Robust'):
            Scaler = RobustScaler()
        colNames = self.dataSet.columns.values.tolist()
        self.dataSet = Scaler.fit_transform(self.dataSet)
        self.dataSet = pd.DataFrame(self.dataSet,columns=colNames)
        #self.dimRed()
        return None


    def dimRed(self, dim = 2):
        """Perform Dim red using PCA"""

        x, y = self.dataSet[:,:(self.dataSet.shape[1]-1)],self.dataSet[:,(self.dataSet.shape[1]-1):]
        Obj = PCA(n_components=dim)
        x = Obj.fit_transform(x)
        self.dataSet = np.hstack((x,y))
        self.dataSet = pd.DataFrame(self.dataSet,columns=['dim1','dim2','price'])
        return None

    def plot3D(self):
        """plot the points in 3D scatter plot"""


        df = self.dataSet
        threedee = plt.figure().gca(projection='3d')

        x = 'para3'
        y = 'para4'
        z = 'price'

        threedee.scatter(df[x], df[y], df[z])
        threedee.set_xlabel(x)
        threedee.set_ylabel(y)
        threedee.set_zlabel(z)
        plt.show()

        return None



    def saveDataset(self):
        """Save the cleaned and processed data as CSV"""

        #Save as csv
        self.dataSet.to_csv("Data/scaled.csv",index=False)


        return None

if __name__ == "__main__":

    Obj = preprocess()
    Obj.readDataset()
    Obj.handleweekdays()
    Obj.clean()
    Obj.saveDataset()



